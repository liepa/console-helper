# -*- encoding : utf-8 -*-
# only for rails console
unless defined? Rails::Server
  class Object
    attr_accessor :e, :u
    def ch(param=nil)
      ActiveRecord::Base.logger = Logger.new(STDOUT)
      if param
        @ch ||= {}
        @ch[param.to_s] ||= ConsoleHelper.new(param)
        @u = @ch[param.to_s].user
        @e = @ch[param.to_s].employee
      end
    end
  end
end