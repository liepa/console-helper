# -*- encoding : utf-8 -*-
class ConsoleHelper

  attr_accessor :employee, :user

  # Finds employee active at today by
  # => full_name
  # Finds user by
  # => login
  def initialize(param)
    # first finds emoloyee
    @employee = Employee.active_at(Date.today).where("full_name LIKE '#{param}'").first

    # if employee not found try user
    if @employee.nil?
      @user = User.find_by_login(param)
      unless @user.nil?
        @employee = @user.employee
      end
    else
      @user = @employee.user
    end

    @employee.user.set_as_current unless @employee.nil?
  end
end